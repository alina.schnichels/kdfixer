# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 17:15:55 2017

@author: Arndt
"""


import os
from lxml import etree

tokenidentifierdict={}

for dirs,subdirs,files in os.walk('../EventFiles/'):
    for file in files:
        if file.endswith("Event.xml") :
            print(file)
            with open(dirs+"/"+file,"r",encoding="utf-8") as myfile:
                mytext=myfile.read().replace("\"\"\"","\"&quot;\"")
                mytext=mytext.replace("LogFile","logfile")
                root = etree.fromstring(bytes(mytext,"utf-8"))
                file=file.replace(".Event.xml","")
                for Token in root.iter("Token"):
                    # print(file,Token.attrib)
                    tokenidentifierdict.setdefault(file,{}).setdefault("FinalText",{}).update({int(Token.attrib["id"]):Token.attrib["tok"]})
        
#%%    
print("Collecting Keystrokes")
KDdict={}
TokKDdict={}
for dirs,subdirs,files in os.walk('../EventFiles/'):
    for file in files:
        if file.endswith("Event.xml") :
            # print(file)
            filename=file.replace(".Event.xml","")
            with open(dirs+"//"+file,"r",encoding="utf-8") as myfile:
                mytext=myfile.read().replace("\"\"\"","\"&quot;\"")
                mytext=mytext.replace("LogFile","logfile")
                root = etree.fromstring(bytes(mytext,"utf-8"))
                # file=file.replace(".Event.xml","")
                for i,Mod in enumerate(root.iter("Mod")):
                    Mod.attrib["chr"]=Mod.attrib["chr"].replace("\n","_")
                    Mod.attrib["chr"]=Mod.attrib["chr"].replace(" ","_")
                    KDdict.setdefault(filename,{}).setdefault(i,{}).setdefault("Id",i)
                    TokKDdict.setdefault(filename,{}).setdefault(int(Mod.attrib["tid"]),{}).setdefault("Id",[]).append(i)
                    for key,value in Mod.attrib.items():
                        if key=="chr" and Mod.attrib["type"]=="Mdel":
                             Mod.attrib["chr"]=f'[{Mod.attrib["chr"]}]'
                        KDdict.setdefault(filename,{}).setdefault(i,{}).setdefault(key,value)
                        TokKDdict.setdefault(filename,{}).setdefault(int(Mod.attrib["tid"]),{}).setdefault(key,[]).append(value)
                       
#%%                            
print("Writing .pzl-files")                           
for translation in TokKDdict:
    writefile=False
    try:
          open("../ManualRealignment/"+translation+".pzl")
          answer=input("Do you really want to overwrite "+translation+".pzl (y/n)")
          if answer=="y":
              writefile=True
         
    except FileNotFoundError:
          writefile=True
    if writefile==True:
        with open("../ManualRealignment/"+translation+".pzl","w",encoding="utf-8") as outfile:
            for i,token in enumerate(sorted(list(TokKDdict[translation].keys()))):
                # token=str(token)
                if token in tokenidentifierdict[translation]["FinalText"]:
                    line=[]
                    header=[]
                    if i==0:
                        header.append("Translation")
                        header.append("CharId")
                        header.append("TTokenId")
                        header.append("TToken")
                        outfile.write("\t".join(header)+"\n")
                    line=[]
                    line.append(translation)
                    line.append("TT")
                    line.append("Chars")
                    line.append(token)
                    line.append(tokenidentifierdict[translation]["FinalText"][token].replace("\"","/\""))
                    
                    for ids in TokKDdict[translation][token]["Id"]:
                        # if KDdict[translation][ids].setdefault("Value","")=="[Return]":
                        #     char="/"
                        if KDdict[translation][ids].setdefault("chr","")=="\"":
                            char="/\""    
                            # print("HEHAHDH")
                        else:
                            char=KDdict[translation][ids]["chr"]
        
                        line.append(char)
                    outfile.write("\t".join([str(x) for x in line])+"\n")
                    line=[]
                    line.append(translation)
                    line.append("TT")           
                    line.append("Ids")
                    line.append(token)
                    line.append(tokenidentifierdict[translation]["FinalText"][token].replace("\"","/\""))
                    for ids in TokKDdict[translation][token]["Id"]:
                        line.append(ids)
                    outfile.write("\t".join([str(x) for x in line])+"\n")
                
                 
            