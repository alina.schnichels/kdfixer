# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 11:42:17 2021

@author: Arndt
"""


     
import os
from lxml import etree

for dirs,subdirs,files in os.walk('../EventFiles/'):
    for file in files:
        if file.endswith("Event.xml") :
            KDdict={}
            Alignmentdict={}
            FixedKDdict={}
            filename=file.replace(".Event.xml","")
            print(file)
            with open(dirs+"//"+file,"r",encoding="utf-8") as infile:
                mytext=infile.read()
                mytext=mytext.replace("LogFile","logfile")

                mytext=mytext.replace("chr=\"\"\"","char=\"&quot;\"")
                
              
                p = etree.XMLParser(remove_blank_text=True)
                root = etree.fromstring(mytext,parser=p)
                for i,Mod in enumerate(root.iter("Mod")):
                    Mod.attrib["chr"]=Mod.attrib["chr"].replace("\n","_")
                    Mod.attrib["chr"]=Mod.attrib["chr"].replace(" ","_")
                    KDdict.setdefault(filename,{}).setdefault(i,{}).setdefault("Id",i)
                    for key,value in Mod.attrib.items():
                        if key=="chr" and Mod.attrib["type"]=="Mdel":
                             Mod.attrib["chr"]=f'[{Mod.attrib["chr"]}]'
                        KDdict.setdefault(filename,{}).setdefault(i,{}).setdefault(key,value)
                                
                for i,Align in enumerate(root.iter("Align")):
                    Alignmentdict.setdefault(filename,{}).setdefault(Align.attrib["tid"],[]).append(Align.attrib["sid"])
                            
#%%
            FixedKDdict=KDdict.copy()
            with open("../ManualRealignment/"+filename+".pzl","r",encoding="utf-8") as infile:
                for i,line in enumerate(infile):
                    line=line.replace("\n","").split("\t")
                    if line[1]=="TT" and line[2]=="Ids":
                        
                        for item in line[5:]:
                            if item!="":
                                item=int(item)
                                
                                if FixedKDdict[filename][item]["tid"]!=(line[3]):
                                    FixedKDdict[filename][item]["tid"]=(line[3])
#%%
            with open(dirs+"//"+file,"r",encoding="utf-8") as infile:
        
                mytext=infile.read()
                mytext=mytext.replace("LogFile","logfile")
        
                mytext=mytext.replace("chr=\"\"\"","char=\"&quot;\"")
                
              
                p = etree.XMLParser(remove_blank_text=True)
                root = etree.fromstring(mytext,parser=p)
                filename=file.replace(".Event.xml","")
                for i,Mod in enumerate(root.iter("Mod")):
                    for key,value in Mod.attrib.items():
                        Mod.attrib[key]=  FixedKDdict[filename][i][key]  
                    try:
                        Mod.attrib["sid"]="+".join(Alignmentdict[filename][FixedKDdict[filename][i]["tid"]])
                    except:
                        pass
            et = etree.ElementTree(root)
            out=etree.tostring(et,encoding="utf-8", pretty_print=True).decode('utf-8')
            out=out.replace("&#10;","&#xA;")
            with open("../FixedEventFiles/"+file,"w",encoding="utf-8") as outfile:
                outfile.write(out)
